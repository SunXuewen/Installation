# 一、安装JDK
## 1、安装JDK1.8
```
yum install -y java-1.8.0-openjdk-devel
```
## 2、配置JDK
修改/etc/profile
```
export JAVA_HOME=/etc/alternatives/java_sdk_1.8.0
export CLASSPATH=.:$JAVA_HOME/jre/lib/rt.jar:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
export PATH=$PATH:$JAVA_HOME/bin
```

# 二、安装ElasticSearch 6.8.0
## 1、导入PGP Key
```
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
```
## 2、新增RPM仓库
新增文件/etc/yum.repos.d/elasticsearch.repo
```
[elasticsearch-6.x]
name=Elasticsearch repository for 6.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```
然后安装
```
yum -y install elasticsearch
```
## 3、设置为自动启动
```
systemctl daemon-reload
systemctl enable elasticsearch.service
systemctl start elasticsearch.service
```
## 4、检查是否运行
```
curl http://localhost:9200
```
## 5、修改配置
修改/etc/elasticsearch/elasticsearch.yml文件
```
http.cors.enabled: true
http.cors.allow-origin: "*"
```
## 6、安装分词和拼音
```
cd /usr/share/elasticsearch/bin
./elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v6.8.0/elasticsearch-analysis-ik-6.8.0.zip
./elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-pinyin/releases/download/v6.8.0/elasticsearch-analysis-pinyin-6.8.0.zip
```

# 三、安装Logstash 6.8.0
## 1、导入PGP Key
```
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
```
## 2、新增RPM仓库
新增文件/etc/yum.repos.d/logstash.repo
```
[logstash-6.x]
name=Elastic repository for 6.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```
然后安装
```
yum -y install logstash
```

# 四、安装Kibana 6.8.0
## 1、导入PGP Key
```
rpm --import https://artifacts.elastic.co/GPG-KEY-elasticsearch
```
## 2、新增RPM仓库
新增文件/etc/yum.repos.d/kibana.repo
```
[kibana-6.x]
name=Kibana repository for 6.x packages
baseurl=https://artifacts.elastic.co/packages/6.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md
```
然后安装
```
yum -y install kibana
```
## 3、设置为自动启动
```
systemctl daemon-reload
systemctl enable kibana.service
systemctl start kibana.service
```
## 4、检查是否运行
```
curl localhost:5601/status
```
