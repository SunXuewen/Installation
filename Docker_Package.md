# OpenOffice 4.1.6 安装
## Dockerfile内容
```
#基础镜像为centos
FROM centos:7

#安装
RUN cd /tmp && \
yum -y install wget && \
wget https://sourceforge.net/projects/openofficeorg.mirror/files/4.1.6/binaries/zh-CN/Apache_OpenOffice_4.1.6_Linux_x86-64_install-rpm_zh-CN.tar.gz && \
tar -xvf Apache_OpenOffice*.tar.gz && \
yum install -y zh-CN/RPMS/*.rpm && \
yum install -y java-1.8.0-openjdk.x86_64 && \
yum clean all && \
rm -f Apache_OpenOffice_4.1.6_Linux_x86-64_install-rpm_zh-CN.tar.gz&& \
rm -Rf zh-CN
 
#暴露接口
EXPOSE 9100
 
#启动服务，占用9100端口
CMD /opt/openoffice4/program/soffice -headless -nofirststartwizard  -accept="socket,host=0.0.0.0,port=9100;urp;"
```
## Build 然后运行
```
docker build -t openoffice:V4.1.6 .
docker run -d --restart=always --name openoffice -p 9100:9100 openoffice:V4.1.6
```