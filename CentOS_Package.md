# 一、CentOS系统
## 1、更新时区设置
```
yum install net-tools ntpdate
ntpdate -u cn.pool.ntp.org
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

```

## 2、开启端口
```
firewall-cmd --zone=public --add-port=8080/tcp –-permanent
firewall-cmd --reload
```

## 3、安装make和gcc等

```
yum -y install gcc automake autoconf libtool make gcc-c++ wget
```

# 二、nginx安装过程
## 1、安装PCRE库
```
wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-8.42.tar.gz
tar -zxvf pcre-8.42.tar.gz
cd pcre-8.34
./configure
make
make install
```
## 2、安装zlib库
```
wget http://zlib.net/zlib-1.2.11.tar.gz
tar -zxvf zlib-1.2.11.tar.gz
cd zlib-1.2.11
./configure
make
make install
```
## 3、安装ssl
```
wget https://www.openssl.org/source/openssl-1.1.1a.tar.gz
tar -zxvf openssl-1.1.1a.tar.gz
```
## 4、安装nginx
```
wget http://nginx.org/download/nginx-1.15.8.tar.gz
tar -zxvf nginx-1.15.8.tar.gz
cd nginx-1.15.8
./configure --sbin-path=/usr/local/nginx/nginx \
--conf-path=/usr/local/nginx/nginx.conf \
--pid-path=/usr/local/nginx/nginx.pid \
--with-http_ssl_module \
--with-pcre=/home/sxw/install/pcre-8.42 \
--with-zlib=/home/sxw/install/zlib-1.2.11 \
--with-openssl=/home/sxw/install/openssl-1.1.1a
make
make install
```
## 5、启动nginx
```
netstat -ano|grep 80
/usr/local/nginx/nginx
```
## 6、设置开机启动
```
cd /lib/systemd/system
vi nginx.service
```
增加如下内容
```
[Unit]
Description=nginx service
After=network.target

[Service]
Type=forking
ExecStart=/usr/local/nginx/nginx
ExecReload=/usr/local/nginx/nginx -s reload
ExecStop=/usr/local/nginx/nginx -s quit
PrivateTmp=true
```
运行如下命令，设置开机启动
```
systemctl enable nginx
```
# 三、Java安装
## 1、下载rpm文件直接安装
```
rpm -ivh jdk-11_linux-x64_bin.rpm
```
如果更新运行
```
rpm -Uvh jdk-11_linux-x64_bin.rpm
```
## 2、查找Java安装路径
```
whereis java  output：/usr/bin/java
ls -lrt /usr/bin/java  output：/usr/bin/java -> /etc/alternatives/java
ls -lrt /etc/alternatives/java  output：/etc/alternatives/java -> /usr/java/jdk-11/bin/java
```
## 3、设置环境变量，java11不需要环境变量


# 八、docker安装
## 1、安装地址[https://docs.docker.com/install/linux/docker-ce/centos/](Docker For Centos)
## 2、安装步骤
### 删除以前版本
```
yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
```
### 设置仓储地址
```
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```
### 安装最新版本
```
yum install docker-ce docker-ce-cli containerd.io
```
### 启动Docker，并设置为开机启动
```
systemctl start docker
systemctl enable docker
```